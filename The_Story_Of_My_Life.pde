/**
 * The Story Of My Life.
 * 
 * @author @deconbatch
 * @version 0.1
 * Processing 3.2.1
 * created 2019.07.21
 * an animation of Vector Field with perlin noise in angle.
 */

void setup() {

  size(720, 720);
  colorMode(HSB, 360, 100, 100, 100);
  smooth();
  noLoop();

}

void draw() {

  float baseHue = random(360.0);
  translate(-width*1.0, -height*1.0);    

  // draw 24fps x 12s animation frames
  for (int i = 0; i < 24 * 12; i++) {

    background(0.0, 0.0, 90.0, 100);
    drawVectorField(baseHue, 3.0, i);

    // draw microscope
    strokeWeight(200.0);
    fill(0.0, 0.0, 0.0, 0.0);
    stroke((baseHue + 90.0) % 360.0, 90.0, 20.0, 100.0);
    ellipse(width * 1.5, height * 1.5, width + 200.0, height + 200.0);
  
    saveFrame("frames/" + String.format("%05d", i) + ".png");

  }
  exit();
}

/**
 * drawVectorField
 * @param  _baseHue   : vector field color.
 * @param  _noiseStep : noise parameter multiple value.
 * @param  _plotStart : start point of drawing.
 */
private void drawVectorField(float _baseHue, float _noiseStep, int _plotStart) {

  // shape
  float plotDiv = 0.015;
  float initDiv = 0.05;
  int   plotLen = 15;

  // color
  float baseSiz   = 3.0;
  float radiusMax = 0.3;

  noFill();
  for (float radius = initDiv; radius <= radiusMax; radius += initDiv) {
    
    float radianDiv = map(radius, initDiv, radiusMax, 20.0, 2.0) * initDiv * PI;
    for (float radian = 0.0; radian < TWO_PI; radian += radianDiv) {

      // initial points makes circle shape
      float xInit = radius * cos(radian + radius) + 1.0;
      float yInit = radius * sin(radian + radius) + 1.0;
      int   sparse = floor((xInit + yInit) * 100.0) % 4;  // make 0, 1, 2, 3 with initial point

      // draw vector field
      float xPoint = xInit;
      float yPoint = yInit;
      float rPoint = 0.0;

      float sHue = (_baseHue + map(noise((xInit - yInit) * 10.0), 0.0, 1.0, 0.0, 180.0)) % 360.0;
      float sSat = map(noise((xInit + yInit) * 10.0), 0.0, 1.0, 40.0, 80.0);
      float sBri = map(noise(xInit * yInit * 10.0), 0.0, 1.0, 20.0, 80.0);
      float sAlp = 100.0;
      strokeWeight(0.1 + 2.0 * noise(radius + radian));
      stroke(sHue, sSat, sBri, sAlp);
      beginShape();
      for (int plotCnt = 0; plotCnt < _plotStart + plotLen; ++plotCnt) {

        // vector field calculation
        float xPrev = xPoint;
        float yPrev = yPoint;
        float rPrev = rPoint;
        rPoint += map(noise(xPrev * _noiseStep, yPrev * _noiseStep), 0.0, 1.0, -1.0, 1.0);
        xPoint += plotDiv * cos(TWO_PI * rPoint);
        yPoint += plotDiv * sin(TWO_PI * rPoint);

        if (plotCnt > _plotStart) {
          float pRatio = map(plotCnt, _plotStart, _plotStart + plotLen, 0.0, 1.0);
          float pSiz   = baseSiz * sin(PI * pRatio);
          ellipse(xPoint * width * 1.5, yPoint * height * 1.5, pSiz, pSiz);
          curveVertex(xPoint * width * 1.5, yPoint * height * 1.5);
        }
      }
      endShape();
    }
  }
}
